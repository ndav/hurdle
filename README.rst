HURDLE
======

Hurdle is a 'tool' to help people cheat at Wordle-like games with style,
grace, and some efficiency. There is no reason for this to exist other than my
personal need to get it out of my head. I am in no way responsible for your
inability to play fair.

Hurdle attempts to produce a list of words that are scored based on arbitrary
criteria like positional frequency of letters, difference from other words in
the list, and presence of duplicate letters. To achieve this stunning feat,
hurdle requires a dictionary file - an ASCII (or Unicode, but see below) file
containing many words, one per line. The standard dictionary file
(/usr/share/dict) on most *nix-like systems is sufficient.

Although letter frequency data can be cached (and is by default), some time is
spent when first using a given dictionary while hurdle examines the file in
order to determine letter frequencies. Naturally, larger dictionaries take
more time. If you'd rather hurdle didn't scribble files onto your filesystem,
the -C flag prevents this caching at the cost of re-calculating frequencies at
every run.

Hurdle understands nothing about parts of speech - a proper noun is the same
as a verb is the same as an acronym as far as it is concerned. Either prune
your dictionary file, use the -D command-line option (perhaps repeatedly), or
just ignore unwanted results. The choice is entirely up to you.

Requirements and Installation
-----------------------------

.. note::
   While there's no technical reason why hurdle will not be perfectly usable
   on, say, Microsoft Windows, the author has no experience with Python in
   those environments. In addition, some assumptions made by hurdle (default
   dictionary location, default file location for caching letter frequency
   data) are *nix-centric. Hurdle's command-line options (-c, -C, -d) may help
   with those things. If anyone wishes to provide additional information about
   usage or issues with hurdle in a specific environment, that'd be great.

Hurdle is written in Python, and is known to work with at least version 3.7
(and very likely later versions). It probably won't fare so well on with
earlier versions, especially of the 2.x variety. There are no external
dependencies.

Installation can be via the standard setuptools process (e.g. "python
./setup.py install"), but is in no way required. All of hurdle is in a single
file which may be run directly::
  ./hurdle.py 5

Don't forget to set the execute bit on hurdle.py, or just feed it to the
interpreter manually::
  python ./hurdle.py 5


Invocation
----------

.. note::
   'hurdle' in the following examples is the default script name as produced
   by setuptools installation. You should replace it by whatever filename
   (and possibly path) you saved the script as if you manually "installed" it.

hurdle is a command-line tool, normally invoked like::

  hurdle [int]

Where <int> is the length (in characters) of the words you'd like hurdle to
produce. The default behavior with no flags or options is to return ten
five-letter words from /usr/share/dict, and store letter frequency data in
"~/.hurdle_cache"

Running 'hurdle --help' should produce the following::
  
    usage: hurdle [-h] [-d FILE] [-w WORD COUNT] [-n LETTERS] [-m LETTERS]
		  [-p REGEX] [-D WORD] [-c FILE] [-C] [-S WORD]
		  [COUNT]

    Generate words based on positional letter frequency and user-provided
    filtering rules

    positional arguments:
      COUNT                 return words of length COUNT

    optional arguments:
      -h, --help            show this help message and exit
      -d FILE, --dict FILE  path to dictionary file (default is
			    "/usr/share/dict/words")
      -w WORD COUNT, --words WORD COUNT
			    number of words to return (default is 10)
      -n LETTERS, --not-in LETTERS
			    string of letters which may not appear in word
      -m LETTERS, --must-have LETTERS
			    string of letters which must appear in word
      -p REGEX, --pattern REGEX
			    optional regex filter expression - e.g. "a..[^f]s"
      -D WORD, --disallowed WORD
			    ignore WORD - this option may be repeated
      -c FILE, --config FILE
			    path to config file (default is "~/.hurdlecache")
      -C, --no-config       do not load or save config (frequency cache)
      -S WORD, --seed-word WORD
			    start with this word in list - may be repeated WORD
			    COUNT times


Here the '-d' option allows to provide a path to a dictionary file of your
choosing. Dictionary files are (in this instance) just a text file containing
words, one word per line. Dictionary files may contain non-ascii (Unicode
whatever) words, but words which contain non-ascii characters will be silently
ignored. This may change in the future.

The '-c' option can be used to tell hurdle where it should store and find its
letter frequency database (which changes for each dictionary and also each
word-length requested). If you'd rather it didn't, the '-C' flag tells hurdle
to run without loading or saving a cache file.

The '-w' option indicates the *maximum* number of words you'd like hurdle to
produce. Fewer words may be produced. The default is ten.

Finally, the remaining '-n', '-m', '-D', and '-p' options are filter
options. These options, in combination, dictate the words hurdle will consider
(and thus produce).

The argument to '-n' is a string of characters (e.g. "-n tuisfq") which **may
not** be present in any of the produced words. Its inverse, '-m', accepts a
string of letters which **must** appear in the resulting words at least
once. Neither of these are positional - the letters are free to appear (or not
appear) anywhere within the resulting word.

The '-D' option is special in that it may be repeated on the command line. It
allows to forbid (an) entire word(s) from being considered. As an example::
  hurdle -D potato -D tomato -D papers 6
will produce up to ten words, none of which may be "potato", "tomato", or
"papers".

Finally, for fine-grained control, the '-p' option allows to provide a regular
expression which considered words must match. Words that do not match the
expression are discarded. As an example::
  hurdle 5 -p "a..[^sd]."

will attempt to produce a list of words that start with the letter 'a', and
have any letter except 's' or 'd' in the fourth position. All other positions
may contain any letter.

Using these four options in various combinations allows filtering the
resultant word list to fit the user's need. Yes, all of the filtering could be
achieved with one gnarly regex, but that's an exercise for someone else.

Note that it's entirely possible to shoot yourself in the foot with
filters. For example::
  hurdle -n q -p "..qu."

will not produce any words, as the regex filter requires a 'q' in the third
position, but the -n flag forbids 'q' from appearing in a word.


Scoring (how words are selected)
--------------------------------

The selection of words is based on a fairly simple (and entirely unverified)
heuristic. The dictionary is iterated in file order, and the words are
filtered according to the criteria privided at the command-line. A word score
is calculated for each of the remaining words. The score is based on letter
frequency by word position (that is, how often a given letter appears in that
position - calculated for all the words in the provided dictionary that match
the word length criteria given on the command-line). For example, if 's'
appears in the fifth position in 500 words out of the (e.g.) 10,000
five-letter words in the dictionary, then an 's' in the fifth position has a
frequency score of 0.05 (500/10000).

For each word, the positional scores of all the letters in that word are
summed. That value is the base word score, and is then multiplied by a sort of
normalized-hamming distance weight by comparing it with all the words
currently in the results list (testing how "different" that word is from the
other words in the list). If the resulting score is greater than the lowest
scoring word in the results list, the new word is added.

Each iteration, the results list is trimmed if necessary by removing the
lowest-scoring words until the length of the list <= the requested word count.

Who Would Waste Time Doing This?
--------------------------------

Hi! Have we met? I write gobs of single-use, niche utility, and otherwise
vaguely pointless code. It's kind of a compulsion. Also, it lets me play
around with new ideas and learn new things.

The code here, like a lot of my stuff, isn't very good. It's almost certainly
not the "right" way to do things. Usually programs like this are an excuse for
me to play around with new ideas or just see if there's a better way to do a
thing.

In the particular case of this application, I didn't find the game itself very
interesting - I'm not very good at self-grep-ing. But the idea of
"statistically best-possible" word hunting seemed like a fun thing to play
with. So I did.

Anyway, it's free - have fun.
