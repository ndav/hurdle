#  -*- fill-column: 78; eval: (auto-fill-mode) -*-
* Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The markup in this ChangeLog is based on Emacs org-mode (https://orgmode.org)

** [Unreleased]

*** Added
    Excessive complexity
    - Mucho documentation (docstrings, argparse help strings, etc.)
    - seed words command-line option and associated logic
      
*** Changed
    Simplicity to complexity
    - JSON output now stores integer values as integer values, instead of
      unnecessary encoding object for Decimals

*** Fixed
    
    - WordFilter now pays attention to 'disallowed' word list if present

*** Removed
    - score_word() - capability merged into FreqData class
    - hamming_dist() - merged into WordDict


** [0.0.0] - 2022-01-16

*** Added
    - Initialized project
