#!/usr/bin/env python
#  -*- fill-column: 78; eval: (auto-fill-mode) -*-
"""Sort some words by entropy

Given a dictionary file (text, one word per line), return an optimal selection
of fixed-length words based on positional letter frequency and uniqueness,
filtered by user-provided rules.

This script requires Python 3.7 or later, but has no external
dependencies. The script may be installed system-wide (or venv-wide) using the
standard setuptools "python ./setup.py install" dance, but installation is not
required. The module may be run as-is from the command line.

N.B. although full Unicode support is possible, the current version does not
fully support it - specifically, the scripts idea of what a "letter" is is
limited to lower-case ASCII text only. Words from the dictionary are
automatically case-folded, but other input may not be. Words containing
non-ASCII letters are at best silently ignored, and at worst may cause
incontinence.
"""
from __future__ import annotations
import argparse
import collections.abc
from decimal import Decimal
import hashlib
import json
import logging
import pathlib
import random
import re
from typing import (Any,
                    Dict,
                    Generator,
                    List,
                    Optional,
                    Sequence,
                    Tuple,
                    Union,)

D = Decimal
logging.basicConfig()
log = logging.getLogger(__name__)

# The only letters we care about, anyway
ALPHABET = 'abcdefghijklmnopqrstuvwxyz'

# Prime the RNG
random.seed()


class DecimalEncoder(json.JSONEncoder):
    """JSON encoder for :class:`decimal.Decimal` objects

    Stores non-integer Decimals as JSON objects: 
    { "__type__"="D", "v"="<decimal as a string>" }
    """
    def default(self, o):
        if isinstance(o, Decimal):
            if not o.is_nan() and o.as_integer_ratio()[1] == 1:
                return int(o)
            else:
                return dict(__type__='D', v=str(o))
        return super(DecimalEncoder, self).default(o)

def _as_decimal(dct: Dict) -> Union[Decimal, Dict]:
    """Handles converting :class:`Decimal` JSON objects back to Decimals"""
    if '__type__' in dct and dct['__type__'] == 'D':
        return Decimal(dct['v'])
    return dct


InitDictType = Union[List[Tuple[Decimal, str]], Dict[Decimal,str]]
class WordDict(collections.abc.MutableMapping):
    """Container for scored words - a kind of multi-dict with randomness

    Stores words and their scores in a size-limited mapping keyed by score as
    a :class:`decimal.Decimal`. Since multiple words may have the same score,
    keys can store multiple values (words) in each key. Internally this is
    handled as a list for each key.

    N.B. If a given key (score) has multiple values (words), __getitem__ and
    __delitem__ will return one (or delete one) *at random*. Likewise when the
    container is full, pruning happens randomly from the lowest-scoring key(s).
    """
    def __init__(self, source: Optional[InitDictType]=None,
                 max_len: Optional[int]=None):
        self._min = 0
        self._d = dict()
        if source is not None:
            if isinstance(source, collections.abc.MutableMapping):
                source = source.items()
            for k, v in source:
                k = D(str(k))
                val = self._d.get(k, [])
                val.append(v)
                self._d[k] = val
                self.min = min(self._min, k)
            self._len = sum([len(v) for v in self._d.values()])
            max_len = self._len
        self.max_len = max_len

    @property
    def max_len(self) -> int:
        """The maximum number of words this list can contain"""
        return self._ml

    @max_len.setter
    def max_len(self, value: int):
        self._ml = value
        self._trim_and_recalc()
        
    def __getitem__(self, key: Decimal) -> str:
        """Returns a *random* selection from the values stored under key"""
        key = D(key)
        if key not in self._d or not self._d[key]:
            self._d.pop(key, None) # Clean up stray empties
            raise KeyError('key')
        return random.choice(self._d[key])

    def __setitem__(self, key: Decimal, value: str):
        key = D(key)
        val = self._d.get(key, [])
        val.append(value)
        self._d[key] = val
        self._len += 1
        self._trim_and_recalc() 

    def __delitem__(self, key: Decimal):
        """Deletes a *random* value from key"""
        key = D(key)
        if key not in self._d or not self._d[key]:
            self._d.pop(key, None) # Clean up stray empties
            raise KeyError(key)
        self._d[key].remove(random.choice(self._d[key]))
        if len(self._d[key]) == 0:
            del self._d[key]
        self._len -= 1
        self._trim_and_recalc()

    def __len__(self):
        return self._len

    def __iter__(self):
        return iter(self._d)

    def __repr__(self):
        return self._d.__repr__()

    def __str__(self):
        return self._d.__str__()

    def words(self) -> Generator[str, None, None]:
        """Generate all words we contain, highest-scoring first"""
        return (w for k in sorted(self._d, reverse=True)
                for w in self._d[k])

    def words_and_scores(self) -> Generator[Tuple[str, Decimal], None, None]:
        """Generate all words we contain and their scores - max score first"""
        return ((w, k) for k in sorted(self._d, reverse=True)
                for w in self._d[k])
    
    def _trim_and_recalc(self):
        """Trim length to self.max_length, and recalculate self._min"""
        self._len = sum([ len(self._d[k]) for k in self._d ])
        while len(self) > self.max_len:
            self._min = min(self._d.keys())
            del self[self._min]
            self._len = sum([ len(v) for v in self._d.values() ])
        if self._len == self.max_len:
            self._min = min(self._d.keys())
        
    def consider_word(self, word: str, score: Decimal) -> bool:
        """Either add the word to the list or discard it
        
        :param word: the word to consider
        :param score: an arbitrary score as a decimal (higher is better)
        :return: True if the word was added to the list, False otherwise

        Compares `score` to the scores of all the other words in this list. If
        `score` > min(list_scores) then the word is added to the list.
        """
        score = D(score)
        if score < self._min:
            return False
        self[score] = word # __setitem__ takes care of pruning and etc.
        return True

    def hamming_dist(self, word: str) -> Decimal:
        """Return the minimum hamming distance between `word` and our words

        :param word: the word to compare
        :return: the minimum Hamming distance between `word` and all the words
            in this :class:`WordDict` as a normalized value [0.0, 1.0] where 0
            is identical to some word in this list, and 1 indicates no leters
            in common with any word in this list
        """
        if not list(self.words()):
            return D(1)
        hams = [ sum([ 0 if a==b else 1 for a,b in zip(word, cword) ])
                 for cword in self.words() ]
        return min(hams)/D(len(word))

    
class WordFilter:
    """Filtering operator for words

    Note __str__, _sig, and _len produce various interpretations of the filter
    as a string, which is used internally.
    """
    def __init__(self, must_contain: Optional[str]=None,
                 must_not_contain: Optional[str]=None,
                 min_length: Optional[int]=None,
                 max_length: Optional[int]=None,
                 omit: Optional[List[str]]=None,
                 regex: Optional[re.Pattern]=None):
        self.must = must_contain
        self.cant = must_not_contain
        self.minl = min_length
        if self.minl and self.minl < 0:
            raise ValueError('minimum word length must be greater than 0')
        self.maxl = max_length
        if (self.maxl is not None
            and self.minl is not None
            and self.maxl < self.minl):
            log.warning('filter minimum word length less than maximum word '
                        f'length - setting to {self.minl}')
            self.maxl = self.minl
        self.omit = omit
        self.rx = regex

    def __call__(self, word: str) -> bool:
        """This is the filter action - returns True if the word given passes"""
        if not self.minl <= len(word) <= self.maxl:
            return False
        if any([ l not in ALPHABET for l in word ]):
            return False
        if self.must and not all([ c in word for c in self.must ]):
            return False
        if self.cant and any([ c in word for c in self.cant ]):
            return False
        if self.rx and not self.rx.match(word):
            return False
        if self.omit and word in self.omit:
            return False
        return True

    def _sig(self):
        return f'{self.must}:{self.cant}:{self.minl}:{self.maxl}'

    def _len(self):
        return f'{self.minl}:{self.maxl}'
    
    def __str__(self):
        return (f'{self.must}:{self.cant}:{self.minl}:{self.maxl}:'
                f'{self.rx.pattern if self.rx else ""}')

    def __repr__(self):
        argmap = [ (kw, val)
                   for kw, val in (('must_contain', self.must),
                                   ('must_not_contain', self.cant),
                                   ('min_length', self.minl),
                                   ('max_length', self.maxl),
                                   ('omit', self.omit),
                                   ('regex', self.rx),) ]
        args = ', '.join([ f'{kw}={val!r}' for kw, val in argmap
                           if val is not None])
        return f'{self.__class__.__name__}({args})'
    
    @classmethod
    def from_string(cls, sig: str) -> WordFilter:
        must, cant, minl, maxl, rx = sig.split(':', 4)
        return cls(must, cant, int(minl), int(maxl), rx)

    
class FreqData:
    """Maintain letter frequency data and provide applications of it

    Keeps indices of letter frequencies tabulated by dictionary source file
    and optional filter. Provides methods to load and save same to a JSON
    file. Identifies dictionary files either by full filesystem path (default
    - faster) or optionally by SHA-256 digest (see `use_hash` parameter to
    :meth:`__call__`).

    Returns a :class:`FreqData.FreqCollection` on call, which may be used as a
    context manager. E.g.:

    >>> fd = FreqData()
    >>> fd.load_cache(pathlib.Path('freq_cache.json')) # Optional
    >>> with fd(pathlib.Path('/usr/share/dict/words')) as coll:
    ...     score = coll.sum_score('llama')

    Words longer than FreqData.MAX_POS characters (default==15) are ignored.

    This is the 300lb gorilla that really does too much, but...
    """
    MAX_POS = 15 # Largest 'word' to consider
    class FreqCollection(collections.abc.Mapping):
        """A collection of frequency data for a given dict and selection size

        May be used directly, or as a context manager. Returned by FreqData()
        """
        def __init__(self, freq_dict: Dict[str, List[Decimal]],
                     max_pos: int):
            self._d = freq_dict
            self._t = D(freq_dict['total_words'])
            self.max_pos = max_pos
            
        def __getitem__(self, key) -> List[Decimal]:
            if key not in LETTERS:
                raise KeyError(key)
            return self._d[key]

        def __iter__(self):
            return iter(self._d)

        def __len__(self):
            return len(self._d)
        
        def __enter__(self):
            return self

        def __exit__(self, *args, **kw):
            pass
        
        def frequency_of(self, letter: str, position: int) -> Decimal:
            """Return the frequency of the given letter at the given position
            """
            if position > self.max_pos:
                raise ValueError('no frequency data for words longer than '
                                 f'{self.max_pos}')
            if letter not in self._d:
                log.warning('no frequency data for character "{letter}"')
            return self._d[letter][position]/self._t

        def sum_score(self, word: str,
                      dup_pen: Decimal=D('0.25')) -> Decimal:
            """Score a word by summing the frequency data of its letters

            :param word: the word we'd like a score for
            :param dup_pen: penalty value for duplicate letters - default is
                0.25 - should be between 0.00001 and 1.0, with lower numbers
                producing a stronger penalty.
            :return: an arbitrary score value that is specific to this
                dictionary and word size - higher scores are better
            """
            lcount = { c: word.count(c)-1 for c in set(word) }
            q = 0
            for i, l in enumerate(word):
                freq = self.frequency_of(l, i)
                dup = dup_pen**lcount[l]
                q += freq*dup
            return q

    def __init__(self, source: Optional[pathlib.Path]=None):
        if source is not None:
            source = pathlib.Path(source).expanduser()
            self._source = source
        self.freq_data = dict()
        self._hash = None

    def __repr__(self):
        return f'{self.__class__.__name__}(source={self._source!r})'

    def load_cache(self, source: pathlib.Path):
        """Load frequency data from a JSON cache"""
        source = pathlib.Path(source).expanduser().resolve()
        if not source.exists():
            log.warning('frequency data cache not found')
            self.freq_data = dict()
            return
        self.freq_data = json.loads(source.read_text(),
                                    parse_int=Decimal,
                                    parse_float=Decimal,
                                    object_hook=_as_decimal)

    def get_dict(self, source: pathlib.Path,
                 filt: Optional[WordFilter]=None,
                 use_hash: bool=False,
                 force: bool=False) -> Dict[str, List[Decimal]]:
        """Select the dictionary to use for calculating letter frequencies etc
        
        :param source: path to the dictionary file
        :param filt: Optional `class`:WordFilter` object - if present, letter
            frequencies are only calculated for matching words. If None, all
            letter frequencies are calculated.
        :param use_hash: identify dictionary by file hash digest (sha256)
            rather than file path (the faster default)
        :param force: if True, reload and examine the dict, ignoring
            (and replacing if saved) any cached data
        :return: a dict containing frequency data by letter and word
            position. Word position is represented as a sequence - e.g. for
            the letter 'k', retval['k']==[freq in first pos, second pos,
            third pos,...]. 
        """
        source = pathlib.Path(source).expanduser().resolve()
        if use_hash:
            fhash = hashlib.sha256()
            with source.open('rb') as fh:
                block = fh.read(8192)
                while block:
                    fhash.update(block)
                    block = dh.read(4096)
            self._hash = fhash.hexdigest()
        else:
            if str(source) in self.freq_data:
                self._hash = self.freq_data[str(source)]
            else:
                self._hash = None
        if (not self._hash
            or self._hash not in self.freq_data
            or (filt and filt not in self.freq_data[self._hash])
            or force):
            if self._hash is not None:
                try:
                    del self.freq_data[self._hash]
                    del self.freq_data[source]
                except KeyError:
                    pass
            with source.open('r') as fh:
                fhash, counts = count_letter_freqs(fh, self.MAX_POS, filt)
            self.freq_data[str(source)] = fhash
            self.freq_data[fhash] = counts
            self._hash = fhash
        else:
            log.debug('using cached frequency data')
        return self.freq_data[self._hash][filt._len()]

    def __call__(self, source: pathlib.Path,
                 filt: Optional[WordFilter]=None,
                 use_hash: bool=False,
                 use_cache: bool=True):
        """Use the dictionary at `source` as the source of frequency data

        :param source: path to the source dictionary file
        :param filt: Optional `class`:WordFilter` object - if present, letter
            frequencies are only calculated for matching words. If None, all
            letter frequencies are calculated.
        :param use_hash: if True, use the file's sha256 hash to identify it in
            the cache - otherwise use the file's full path (faster).
        :param use_cache: if False, ignore (and replace) any cached data for
            this dictionary, and re-calculate letter frequencies
        """ 
        d = self.get_dict(source, filt, use_hash, not use_cache)
        return self.FreqCollection(d, self.MAX_POS)
       
    def save_cache(self, outf: pathlib.Path):
        """Save the letter frequency data to the given file as JSON

        :param outf: file in which to save the data - will be over-written
        """
        if not outf:
            log.debug('no cache path provided to save_cache() - not saving')
            return
        if not self.freq_data:
            log.warning('no frequency data to save - not saving')
            return
        outf = pathlib.Path(outf).expanduser().resolve()
        with outf.open('w') as fh:
            json.dump(self.freq_data, fh, cls=DecimalEncoder, indent=2)

        
    
def count_letter_freqs(source: Sequence[Sequence],
                       max_len: int=10,
                       filt: Optional[WordFilter]=None) \
    -> Tuple[str, Dict[str, List[Decimal]]]:
    """Count positional letter frequencies from the given source

    :param source: some iterable collection of words
    :param max_len: the maximum length of a word - words longer than this in
        `source` are silently ignored
    :param filt: optional filter - if provided, the min/max word lengths are
        used to identify words which will be counted specially
    :return: a tuple containing the hash (sha256) of the *entire* source list
        including filtered words, and a dictionary of letter frequencies
    """
    counts = { None: { l: [D(0)]*max_len
                       for l in ALPHABET } }
    filtid = filt._len() if filt else False
    if filt:
        counts[filtid] = { l: [D(0)]*filt.maxl
                           for l in ALPHABET }
    fhash = hashlib.sha256()
    filt_words = words = 0
    for line in source:
        fhash.update(line.encode('utf-8'))
        word = line.strip().lower()
        if len(word) > max_len:
            continue
        # We ignore words with non-letters in them (-, ', etc.)
        if not all([ c in ALPHABET for c in word ]):
            continue
        for i, l in enumerate(word):
            counts[None][l][i] += 1
            if filt and filt(word):
                counts[filtid][l][i] += 1
                filt_words += 1
        words += 1
    counts[None]['total_words'] = words
    if filt:
        counts[filtid]['total_words'] = filt_words
    return (fhash.hexdigest(), counts)

def init_cmdline_parser(parser: Optional[argparse.ArgumentParser]=None) \
    -> argparse.ArgumentParser:
    """Set up argparse for command-line parsing"""
    p = parser if parser is not None else argparse.ArgumentParser()
    p.add_argument('-d', '--dict', type=pathlib.Path, default=None,
                   required=False, metavar='FILE', help='path to dictionary '
                   'file (default is "/usr/share/dict/words")')
    p.add_argument('-w', '--words', type=int, default=10, metavar='WORD COUNT',
                   help='number of words to return (default is 10)')
    p.add_argument('-n', '--not-in', type=str, default='', metavar='LETTERS',
                   help='string of letters which may not appear in word')
    p.add_argument('-m', '--must-have', type=str, default='',
                   metavar='LETTERS',
                   help='string of letters which must appear in word')
    p.add_argument('-p', '--pattern', type=str, default='', metavar='REGEX',
                   help='optional regex filter expression - e.g. "a..[^f]s"')
    p.add_argument('-D', '--disallowed', type=str, action='append',
                   metavar='WORD', default=[], help='ignore WORD - this '
                   'option may be repeated')
    p.add_argument('-c', '--config', type=pathlib.Path, dest='rcfile',
                   default=pathlib.Path('~/.hurdle_cache'), metavar='FILE',
                   help='path to config file (default is "~/.hurdlecache")')
    p.add_argument('-C', '--no-config', action='store_true', default=False,
                   help='do not load or save config (frequency cache)')
    p.add_argument('-S', '--seed-word', type=str, action='append', default=[],
                   metavar='WORD', help='start with this word in list - '
                   'may be repeated WORD COUNT times')
    p.add_argument('letter_count', type=int, metavar='COUNT', default=5,
                   nargs='?', help='return words of length COUNT')
    return p

def parse_cmdline(parser: argparse.ArgumentParser) -> argparse.Namespace:
    """Parse the command-line, validate and make corrections to user input"""
    args = parser.parse_args()
    if args.dict is None:
        args.dict = pathlib.Path('/usr/share/dict/words')
    dis = [ w.lower() for w in args.disallowed
            if len(w) == args.letter_count ]
    if len(dis) != len(args.disallowed):
        miss = list(set(args.disallowed) - set(dis))
        pl = len(miss)>1
        if not pl:
            miss = miss.pop()
        log.warning(f'ignoring disallowed word{"s" if pl else ""}: '
                    f'{miss} - incorrect length')
        args.disallowed = dis
    args.not_in = args.not_in.lower()
    args.must_have = args.must_have.lower()
    huh = list(set(args.not_in).intersection(set(args.must_have)))
    if huh:
        lets = ' and '.join((', '.join([ f'"{l}"' for l in huh[:-1] ]),
                             f'"{huh[-1]}"'))
        p = ('s', 'are') if len(huh)>1 else ('', 'is')
        log.warning(f'the letter{p[0]} {lets} {p[1]} simultaneously required '
                    'and prohibited by the command line options')
    if len(args.seed_word) > args.words:
        log.warning(f'provided seed words [{len(args.seed_word)}] exceed '
                    f'length of word list [{args.words}]')
        log.warning('trimming extra seed words')
        args.seed_word = args.seed_word[:args.words]
    return args

def main():
    """Main script entry-point"""
    parser = argparse.ArgumentParser(
        description='Generate words based on positional letter frequency '
        'and user-provided filtering rules')
    parser = init_cmdline_parser(parser)
    args = parse_cmdline(parser)
    log.debug(f'command line arguments: "{args}"')
    filt = WordFilter(args.must_have, args.not_in,
                      args.letter_count, args.letter_count,
                      args.disallowed,
                      re.compile(args.pattern))
    res = WordDict(max_len=args.words)
    corpus = FreqData(args.dict)
    if not args.no_config:
        corpus.load_cache(args.rcfile)
    with corpus(args.dict, filt) as freq:
        for w in args.seed_word: # Plant seed words
            if not filt(w):
                log.warning(f'seed word "{w}" prohibited by filter - omitted')
            else:
                score = freq.sum_score(w)*res.hamming_dist(w)
                res.consider_word(w, score)
        with args.dict.open('r') as fh:
            for line in fh:
                word = line.strip().lower()
                if filt(word):
                    score = freq.sum_score(word)*res.hamming_dist(word)
                    res.consider_word(word, score)
    spacing = args.letter_count + 5
    print(f'{"WORD":<{spacing}s}SCORE')
    for w, s in res.words_and_scores():
        print(f'{w:<{spacing}s}{s:02.4f}')
    if not args.no_config:
        corpus.save_cache(args.rcfile)

# Yeah, you can run the script directly
if __name__ == '__main__':
    main()

